<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200905_043014_creat_table_score
 */
class m200905_043014_creat_table_score extends Migration
{
    public function up()
    {
        $this->createTable('score', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
            'sender_id' => $this->integer()->notNull(),
            'recipient_id' => $this->integer()->notNull(),
            'sum' => $this->float()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'purpose_of_payment' => $this->string(255)
        ]);
    }

    public function down()
    {
        $this->dropTable('score');
    }
}
