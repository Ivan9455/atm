<?php

use yii\db\Migration;

/**
 * Class m200907_044729_add_fk_score
 */
class m200907_044729_add_fk_score extends Migration
{
    public function up()
    {
        $this->addForeignKey(
            'fk-score-sender_id',
            'score',
            'sender_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-score-recipient_id',
            'score',
            'recipient_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-score-sender_id',
            'score'
        );

        $this->dropForeignKey(
            'fk-score-recipient_id',
            'score'
        );
    }
}
