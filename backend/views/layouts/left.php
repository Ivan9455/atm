<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'items' => [
                    ['label' => 'Score', 'url' => ['/score'], 'icon' => 'fas fa-book'],
                ],
            ]
        ) ?>
    </section>
</aside>
