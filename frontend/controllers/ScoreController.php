<?php

namespace frontend\controllers;

use common\models\Score;
use yii\rest\ActiveController;
use yii\web\ServerErrorHttpException;
use yii\helpers\Url;
use Yii;

class ScoreController extends ActiveController
{
    public $modelClass = 'common\models\Score';

    /**
     * @return array
     */
    public function actions() : array
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $model = new Score(Yii::$app->getRequest()->post('Score'));

        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set(
                'Location',
                Url::toRoute(['view', 'id' => $id], true)
            );
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}
