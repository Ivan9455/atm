<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ScoreAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    public $js = [
        'js/score.js'
    ];
}
