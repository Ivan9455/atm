$(document).ready(function () {
    $('.create_score_btn').on('click', function () {
        $.ajax({
            type: "POST",
            url: "/score/create",
            data: $('#score-form').serializeArray(),
            error: function (result) {
                if (result.status === 422) {
                    jQuery('#w2-success-0').remove();
                    for (let i = 0; i < result.responseJSON.length; i++) {
                        let input = jQuery('#score-' + result.responseJSON[i].field)
                        if (input.parent().hasClass('has-success')) {
                            input.parent().removeClass('has-success');
                        }
                        input.parent().addClass('has-error');
                        input.parent().children('.help-block').html(result.responseJSON[i].message)
                    }
                }
            },
            success: function () {
                jQuery('#w2-success-0').remove();
                let template = '' +
                    '<div id="w2-success-0" class="alert-success alert fade in">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    'Success create Score!'
                '</div>'
                jQuery('.wrap > .container').prepend(template)
            }
        })
    })
});
