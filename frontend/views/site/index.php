<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Score */
/* @var $form yii\widgets\ActiveForm */
\frontend\assets\ScoreAsset::register($this);
?>

<div class="score-form">

    <?php $form = ActiveForm::begin(['id' => 'score-form']); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'sender_id')->textInput() ?>

    <?= $form->field($model, 'recipient_id')->textInput() ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'purpose_of_payment')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::button('Save', ['class' => 'btn btn-success create_score_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
