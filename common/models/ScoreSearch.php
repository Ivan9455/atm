<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class ScoreSearch extends Score
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'number', 'sender_id', 'recipient_id'], 'integer'],
            [['sum'], 'number'],
            [['created_at', 'purpose_of_payment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Score::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'sender_id' => $this->sender_id,
            'recipient_id' => $this->recipient_id,
            'sum' => $this->sum,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'purpose_of_payment', $this->purpose_of_payment]);

        return $dataProvider;
    }
}
