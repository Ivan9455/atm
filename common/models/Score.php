<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "score".
 *
 * @property int $id
 * @property int $number
 * @property int $sender_id
 * @property int $recipient_id
 * @property float $sum
 * @property string $created_at
 * @property string|null $purpose_of_payment
 */
class Score extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'sender_id', 'recipient_id', 'sum'], 'required'],
            [['number', 'sender_id', 'recipient_id'], 'integer'],
            [['sum'], 'number'],
            [['created_at'], 'safe'],
            [['purpose_of_payment'], 'string', 'max' => 255],
            [
                ['sender_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'message' => 'Sender not exist',
                'targetAttribute' => [
                    'sender_id' => 'id',
                ]
            ],
            [
                ['recipient_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'message' => 'Recipient not exist',
                'targetAttribute' => [
                    'recipient_id' => 'id'
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'sender_id' => 'Sender ID',
            'recipient_id' => 'Recipient ID',
            'sum' => 'Sum',
            'created_at' => 'Created At',
            'purpose_of_payment' => 'Purpose Of Payment',
        ];
    }
}
