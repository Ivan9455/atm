<?php

use common\models\User;
$date = time();
return [
    [
        'username' => 'user1',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ1',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user1@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user2',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ2',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user2@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user3',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ3',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user3@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user4',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ4',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user4@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user5',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ5',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user5@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user6',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ6',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user6@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user7',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ7',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user7@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user8',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ8',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user8@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user9',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user9@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user10',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user01@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user11',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user011@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user12',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user012@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user13',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user013@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user14',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user014@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ],
    [
        'username' => 'user15',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => Yii::$app->security->generatePasswordHash('123456az'),
        'email' => 'user015@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => $date,
        'updated_at' => $date
    ]
];
